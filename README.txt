
Description
-----------
Module to listen to the progress of tests and execute shell commands when
tests start and finish.

Installation
------------
0) Install the core patches found in http://drupal.org/node/407294

1) Place this module directory in your "modules" folder (this will usually be
"sites/all/modules/"). Don't install your module in Drupal core's "modules"
folder, since that will cause problems and is bad practice in general. If
"sites/all/modules" doesn't exist yet, just create it.

2) Enable the module.

3) Go to "admin/build/testlistener" and enter the shell commands you want to
execute when tests start or finish.

Author
------
Litrik De Roy - Norio

* Website: http://www.norio.be/
* Contact: http://www.norio.be/contact
 
